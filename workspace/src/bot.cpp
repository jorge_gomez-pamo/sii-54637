#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include "./../include/DatosMemCompartida.h"


void bot1()
{

}

int main(){
	
	double centro;
	
	////////////////////////bot2/////////////////////////////////////////
	DatosMemCompartida *p_mem2;
	int fd2;

	fd2 = open("/tmp/fichero2", O_RDWR);
	
	if(fd2 < 0){
		printf("error file could not be opened\n");
		return 0;}
	printf("file opened correctly");
	
	p_mem2 = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0));
	
	if(p_mem2 == MAP_FAILED){
		perror("Error al proyectar el fichero en memoria");
		close(fd2);
		return 0;
	}
	
	close(fd2);
	
	/////////////////////////bot1////////////////////////////////////////
	
	DatosMemCompartida *p_mem1;
	int fd1;

	fd1 = open("/tmp/fichero1", O_RDWR);
	
	if(fd1 < 0){
		printf("error file could not be opened\n");
		return 0;}
	printf("file opened correctly");
	
	p_mem1 = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0));
	
	if(p_mem1 == MAP_FAILED){
		perror("Error al proyectar el fichero en memoria");
		close(fd1);
		return 0;
	}
	
	close(fd1);
	
	
	
	while(p_mem1 != NULL && p_mem2 != NULL)
	{
		centro = (p_mem1->raqueta.y2 + p_mem1->raqueta.y1)/2;
		
		if(p_mem1->esfera.centro.y > centro)
			p_mem1->accion = 1;
		else if(p_mem1->esfera.centro.y < centro)
			p_mem1->accion = -1;
		else
			p_mem1->accion = 0;
			
		centro = (p_mem2->raqueta.y2 + p_mem2->raqueta.y1)/2;
		if(p_mem2->esfera.centro.y > centro)
			p_mem2->accion = 1;
		else if(p_mem2->esfera.centro.y < centro)
			p_mem2->accion = -1;
		else
			p_mem2->accion = 0;	
		usleep(250000);		
	}
	
	unlink("/tmp/fichero1");
	unlink("/tmp/fichero2");
	unlink("/tmp/fichero2");
	return 1;
}
