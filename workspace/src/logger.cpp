#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(){
	
	int fd;
	int res = mkfifo("/tmp/myfifo", 0600);
	//message is 50 + number digits long
	
	if(res < 0){
		perror("Error when creating fifo");
		return 0;
	}
		
	
	if((fd = open("/tmp/myfifo", O_RDONLY)) < 0){
		perror("Error when opening fifo");
		return 0;}
	
	char data;
	while(1)
	{
		if(read(fd, &data, 1) > 0){
			if(data == '1'){
				read(fd, &data, 1);
					printf("Jugador 1 marca 1 punto, lleva un total de %c puntos \n", data);}
			else if(data == '2'){
				read(fd, &data, 1);
					printf("jugador 2 marca 1 punto, lleva un total de %c puntos \n", data);}
			else if(data == 'f')
				break;
		}
	}
	
	close(fd);
	
	unlink("/tmp/myfifo");
	return 1;
}
