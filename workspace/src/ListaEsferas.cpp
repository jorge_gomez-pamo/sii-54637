
#include "ListaEsferas.h"

ListaEsferas::ListaEsferas()
{
	numero = 0;
	for (int i = 0; i < MAX_ESFERAS; i++)
		lista[i] = 0;
}

bool ListaEsferas::agregar(Esfera* e)
{
	bool okay = 1;
	for (int i = 0; i < numero; i++)
	{
		if (e == lista[numero])
			okay = 0;
	}
	if (numero < MAX_ESFERAS and okay)
		lista[numero++] = e;
	else
		return false;
	return true;
}

void ListaEsferas::dibuja()
{
	for (int i = 0; i < numero; i++)
		lista[i]->Dibuja();
}

void ListaEsferas::mueve(float t)
{
	for (int i = 0; i < numero; i++)
		lista[i]->Mueve(t);
}

void ListaEsferas::destruirContenido()
{
	for (int i = 0; i < numero; i++)
		delete lista[i];
	numero = 0;
}

void ListaEsferas::eliminar(int index)
{
	if ((index < 0) or (index >= numero))
		return;
	delete lista[index];
	numero--;
	for (int i = index; i < numero; i++)
		lista[i] = lista[i + 1];
}

void ListaEsferas::eliminar(Esfera* e)
{
	for (int i = 0; i < numero; i++)
		if (lista[i] == e)
		{
			eliminar(i);
			return;
		}
}

Esfera* ListaEsferas::operator[](int i)
{
	if (i >= numero) // si el usuario busca una esfera en un hueco que no existe le damos la �ltima de la lista
		i = numero - 1;
	if (i < 0)
		i = 0;
	return lista[i];
}
