// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCliente::CCliente()
{
	Init();
}

CCliente::~CCliente()
{
	munmap(p_mem1, sizeof(mem1));
	munmap(p_mem2, sizeof(mem2));
	unlink("/tmp/fichero1");
	unlink("/tmp/fichero2");
	//close(fdsc);
	unlink("/tmp/fifosc");
	//close(fdcs);
	unlink("/tmp/fifocs");
}

void CCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CCliente::OnTimer(int value)
{	
	//////////////leer datos del servidor desde la tuberia///////////////////
	time += 0.025;
	char cad[200];
	
	//read(fdsc, cad, sizeof(cad));
	s_comun.Receive(cad, sizeof(cad));
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d %d", &esfera.centro.x, &esfera.centro.y, &esfera.radio, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2, &flag_time);
	
	///////////////////////////
	
	if(flag_time == 1){
		time = 0.0;
		flag_time = 0;
		}
	printf("%f", time);
	p_mem2->esfera = esfera;
	p_mem2->raqueta = jugador2;
	
	if(p_mem2->accion == -1)	
		//jugador2.velocidad.y = -4;
		OnKeyboardDown('l', 0, 0);
	else if(p_mem2->accion == 1)
		//jugador2.velocidad.y = 4;
		OnKeyboardDown('o', 0, 0);
	//else
		//jugador2.velocidad.y = 0;
	
	p_mem1->esfera = esfera;
	p_mem1->raqueta = jugador1;
	if(time > 5.0){
		if(p_mem1->accion == -1)	
			//jugador1.velocidad.y = -4;
			OnKeyboardDown('s', 0, 0);
		else if(p_mem1->accion == 1)
			//jugador1.velocidad.y = 4;
			OnKeyboardDown('w', 0,0);
		//else
			//jugador1.velocidad.y = 0;
	}
	

}

void CCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char Key = '0';
	switch(key)
	{
	case 's':jugador1.velocidad.y=-4; Key = 's'; break;
	case 'w':jugador1.velocidad.y=4;  Key = 'w'; break;
	case 'l':jugador2.velocidad.y=-4; Key = 'l'; break;
	case 'o':jugador2.velocidad.y=4;  Key = 'o'; break;
	}
	//write(fdcs, &key, sizeof(key));
	s_comun.Send(&Key, sizeof(Key));
}

void CCliente::Init()
{
	flag_time = 0;
	char nombre[50];
	
	//////////////////////////////socket conexion ////////////////////////////
	printf("Introduzca tu nombre: ");
	scanf("%s", nombre);
	s_comun.Connect("127.0.0.1", 8000);
	s_comun.Send(nombre, sizeof(nombre));
	
	/////////////////bot2//////////////////////////////
	fd2 = open("/tmp/fichero2", O_RDWR|O_CREAT|O_TRUNC, 0666);
	
	if(fd2 < 0){
		perror("error could not create file");
		return;}
	
	mem2.esfera = esfera;
	mem2.raqueta = jugador2;
	mem2.accion = 0;
	
	write(fd2, &mem2, sizeof(mem2));
	
	p_mem2 = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0));
	
	if(p_mem2 == MAP_FAILED){
		perror("Error al proyectar el fichero en memoria");
		close(fd2);
		return;
	}
	
	close(fd2);
	
	///////////////////////////////bot1//////////////////////////////////////////////
	
	fd1 = open("/tmp/fichero1", O_RDWR|O_CREAT|O_TRUNC, 0666);
	
	if(fd1 < 0){
		perror("error could not create file");
		return;}
	
	
	mem1.esfera = esfera;
	mem1.raqueta = jugador1;
	mem1.accion = 0;
	
	write(fd1, &mem1, sizeof(mem1));
	
	p_mem1 = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0));
	
	if(p_mem1 == MAP_FAILED){
		perror("Error al proyectar el fichero en memoria");
		close(fd1);
		return;
	}
	
	close(fd1);
	
	
	///////////////////////////////continuacion/////////////////////////////////////
	time = 0.0;
	Plano p;
	Esfera esfera;

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//sleep(5);
}
