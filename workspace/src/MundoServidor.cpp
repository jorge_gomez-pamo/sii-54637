// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CServidor::CServidor()
{
	Init();
}

CServidor::~CServidor()
{
	write(fd, &end_signal, 1);
	write(fd, &end_signal, 1);
	close(fd);
	//close(fdsc);
	//close(fdcs);
}

void CServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
		jugador1.Rebota(esfera);
		jugador2.Rebota(esfera);
		
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char j = '2';
		char p = 48 + puntos2;
		write(fd, &j, 1);
		write(fd, &p, 1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		char j = '1';
		char p = 48 + puntos1;
		write(fd, &j, 1); 
		write(fd, &p, 1);
		/*write(fd, &message[0], 43); 
		write(fd, &puntos1, sizeof(int));
		write(fd, &message2[0], 7);*/
		//fprintf(fd, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
	}
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d %d", esfera.centro.x, esfera.centro.y, esfera.radio, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2, flag_time);
	//write(fdsc, cad, sizeof(cad));
	s_comun.Send(cad, sizeof(cad));
	flag_time = 0;
}

void CServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
	flag_time = 1;
}

void CServidor::RecibeComandosJugador()
{
	int num_read;
	while(1){
		usleep(10);
		char cad[100];
		//num_read = read(fdcs, cad, sizeof(cad));
		s_comun.Receive(cad, sizeof(cad));
		/*if(num_read < 0){
			perror("Error when readinf the fifo");
			close(fdcs);
			exit(1);
		}*/
		unsigned char key;
		sscanf(cad, "%c", &key);
		if(key == 's') jugador1.velocidad.y = -4;
		if(key == 'w') jugador1.velocidad.y = 4;
		if(key == 'l') jugador2.velocidad.y = -4;
		if(key == 'o') jugador2.velocidad.y = 4;
	}
}

void* hilo_comando(void* d)
{
	CServidor* p=(CServidor*) d;
	p->RecibeComandosJugador();
}

void CServidor::Init()
{	
	flag_time = 0;
	char nombre[50];
	///////////////// socket conexion/////////////////////////
	
	if(s_conex.InitServer("127.0.0.1", 25565) == -1)
		perror("Error al iniciar el socket conexion servidor\n");
	s_comun = s_conex.Accept();
	
	s_comun.Receive(nombre, sizeof(nombre));
	printf("Se ha unido %s\n", nombre);
	
	///////////////// socket comunicacion//////////////////////
	
	///////////////////////////crear thread/////////////////////////////////
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, &attr, hilo_comando, this);
	
	///////////////////////////logger////////////////////////////////
	
	if ((fd = open("/tmp/myfifo", O_WRONLY))< 0){
		perror("error, file myfifo not found");
		return;
		}
	
	Plano p;
	Esfera esfera;

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//sleep(5);
}
