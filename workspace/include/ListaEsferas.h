#pragma once
#include "Esfera.h"
#define MAX_ESFERAS 10
class ListaEsferas
{
public:
	ListaEsferas();
	bool agregar(Esfera* e);
	void destruirContenido();
	void dibuja();
	void mueve(float t);
	void eliminar(int);
	void eliminar(Esfera*);
	int GetNumero() { return numero;}
	Esfera* operator [](int);
	
	Esfera* lista[MAX_ESFERAS];
	int numero;
};
