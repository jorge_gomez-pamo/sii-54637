#pragma once

#include "Esfera.h"
#include "Raqueta.h"
#include <unistd.h>

class DatosMemCompartida
{
	public:
	Esfera esfera;
	Raqueta raqueta;
	int accion;
};
