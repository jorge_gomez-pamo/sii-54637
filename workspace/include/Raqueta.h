// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	Raqueta(){velocidad.x = velocidad.y = 0.0;}
	//~Raqueta();

	void Mueve(float t);
};
